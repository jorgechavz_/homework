defmodule AlienTest do
  use ExUnit.Case
  doctest Alien

  @lines ["3 5 4","abc","bca","dac","dbc","cba","(ab)(bc)(ca)","abc","(abc)(abc)(abc)","(zyx)bc"]
  @test_result "Case #1: 2\nCase #2: 1\nCase #3: 3\nCase #4: 0\n"


  test "should it get L D and N values from a list" do
    line = "3 5 4"
    [l, d, n] = Alien.get_ldn(line)
    assert l == 3
    assert d == 5
    assert n == 4
  end

  test "should it get all the tokens from lines" do
    tokens = Alien.get_tokens @lines
    assert Enum.count(tokens) == 5
    assert tokens == ["abc", "bca", "dac", "dbc", "cba"]
  end

  test "should it get all the test cases" do
    cases = Alien.get_cases @lines
    assert Enum.count(cases) == 4
    assert cases == ["(ab)(bc)(ca)", "abc", "(abc)(abc)(abc)", "(zyx)bc"]
  end

  test "should it transform a test case into a regexp and match it with the tokens" do
    tokens = Alien.get_tokens @lines
    res = Alien.test_case(tokens, "(ab)(bc)(ca)")
    assert res == 2
  end

  test "should read file, execute all cases and print result" do
    {:ok, path_list } = :file.get_cwd()
    path = path_list |> to_string
    sample = Path.join(path,"test/sample.txt")
    {:ok, content} = File.read(sample)
    result = Alien.process(content)
    assert result == @test_result
  end


end
