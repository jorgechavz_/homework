defmodule Alien do

  def get_ldn(line) do
    Enum.map(String.split(line), fn x -> String.to_integer(x)  end)
  end

  def get_tokens(lines) do
    [head | _] = lines
    [_, d, _] = get_ldn(head)
    [_ | tail] = lines
    Enum.slice(tail, 0, d)
  end

  def get_cases(lines) do
    [head | _] = lines
    [_, d, _] = get_ldn(head)
    [_ | tail] = lines
    Enum.slice(tail, d, Enum.count(lines))
  end

  def test_case(tokens, test) do
    regexp = get_regexp(test)
    Enum.reduce(tokens,0, fn (token, acc) ->
      if Regex.match?(regexp, token) do
        acc + 1
      else
        acc
      end
    end)
  end

  def process(content) do
    enum = String.split(content, "\n")
    content = Enum.take(enum, Enum.count(enum)-1)
    tokens = get_tokens(content)
    cases_with_index = get_cases(content) |> Enum.with_index()
    Enum.map(cases_with_index, fn({c,n}) ->
      a = test_case(tokens, c)
      "Case ##{n+1}: #{a}\n"
    end) |> Enum.join()
  end
  
  def demo() do
    {:ok, content} = File.read("test/sample.txt")
    process(content)
  end

  defp get_regexp(text) do
    {:ok, regexp} = String.replace(text,"(","[")
    |> String.replace(")","]")
    |> Regex.compile()
    regexp
  end
end
