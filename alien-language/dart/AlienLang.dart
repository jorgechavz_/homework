import 'dart:io';

main() {
  //Declare ouput file
  final filename = 'output.txt';
  var sink = new File(filename).openWrite();

  //Start reading file line by line
  new File('input.txt').readAsLines().then((List<String> lines) {

    //Get line 0 and split it
    var fileVariables = lines[0].split(" "),
        regExpr = [];

    //set each file variables
    int wordLength = int.parse(fileVariables[0]),
        languageLength = int.parse(fileVariables[1]),
        testCases = int.parse(fileVariables[2]); //L D N

    //
    var languageWords = [];
    var solutions = new List(testCases);

    //Add the language that will be tested.
    for(int i=0; i<languageLength; i++){
      languageWords.add(lines[i+1]);
    }

    //Iterate through each String regex.
    for(int i=0; i<testCases; i++){
      regExpr.add(lines[i+languageLength+1]);
      regExpr[i] = regExpr[i].replaceAll("(","[").replaceAll(")","]");
    }

    
    for(int r = 0; r < regExpr.length; r++){
      regExpr[r] = new RegExp(regExpr[r]);
      solutions[r]=0;
      for(int t = 0; t <languageWords.length; t++){
        if(regExpr[r].hasMatch(languageWords[t])){
          solutions[r]++;
        }
      }
      sink.write("Case #${r+1}: ${solutions[r]}\n");
    }
  });
}
