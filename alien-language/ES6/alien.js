"use strict";

class Alien {

  constructor(inputFile,outputFile) {
    this.inputFile = inputFile;
    this.outputFile = outputFile;
    this.wordLength;
    this.languageLength;
    this.testCases;
  }

  init(evt) {

    if(window.File && window.FileReader && window.FileList && window.Blob){

      var input_file = evt.target.files[0]

      if(input_file){

        let file = new FileReader()

        file.onload = (e) => {

          let lines = e.target.result.split(/\r?\n/);

          let firstLine = lines[0].split(" ");

          this.wordLength = firstLine[0]
          this.languageLength = firstLine[1]
          this.testCases = firstLine[2]

          let possibleCases = lines.slice(1,this.languageLength)

          let regExpr = [];
          var languageWords = [];
          var solutions = [];

          for(var i=0; i < this.languageLength; i++){ //Add the language that will be tested.
            languageWords.push(lines[i+1]);
          }


          for (var i = 0; i < this.testCases; i++) {

            var j = parseInt(i)+parseInt(this.languageLength)+parseInt(1);

            regExpr.push(lines[j])

            let reLeft = new RegExp("\\(",'g');
            let reRight = new RegExp("\\)",'g');

            regExpr[i] = regExpr[i].replace(reLeft,"[").replace(reRight,"]");

          }

          for(var r = 0; r < regExpr.length; r++){
              regExpr[r] = new RegExp(regExpr[r]);
              solutions[r] = 0;
              for(var t = 0; t < languageWords.length; t++){
                if(regExpr[r].test(languageWords[t])){
                  solutions[r]++;
                }
              }
              console.log("Case #"+parseInt(r+1)+": "+solutions[r]+"\n");
            }
        }

        file.readAsText(input_file)

      }

    }else{
      alert("File API are not fully supported in this browser")
    }
  } // end init

}


let inputFile = "small-input.txt"
let outputFile = "output.txt"

var alien = new Alien(inputFile, outputFile)

document.getElementById('fileinput').addEventListener('change', alien.init, false);
