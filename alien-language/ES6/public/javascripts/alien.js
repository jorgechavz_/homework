"use strict";

var Alien = Alien || {};

Alien.App = {
  init: function(content) {
    let lines = content.split(/\r?\n/);
    let ldn = this.getLDN(content[0]);
    let possibleCases = content.slice(1,+content.length-1);
    let tokens = this.getTokens(possibleCases);
    let cases = this.getCases(possibleCases);
  },
  getLDN: function(line) {
    return line.split(" ")
  },
  getTokens: function(lines) {
    console.log(lines);
  },
  getCases: function(lines) {

  },
  testCase: function(tokens, test) {

  },
  getRegex: function(text){
    let reLeft = new RegExp("\\(",'g');
    let reRight = new RegExp("\\)",'g');
    text = text.replace(reLeft,"[").replace(reRight,"]");
  }
}

class Alienn {

  constructor(inputFile,outputFile) {
    this.inputFile = inputFile;
    this.outputFile = outputFile;
    this.wordLength;
    this.languageLength;
    this.testCases;
  }

  init(evt) {

    if(window.File && window.FileReader && window.FileList && window.Blob){
      var input_file = evt.target.files[0]
      if(input_file){
        let file = new FileReader()

        file.onload = (e) => {
          let content = e.target.result;
          Alien.App.init(content);

          let firstLine = lines[0].split(" ");

          this.wordLength = firstLine[0]
          this.languageLength = firstLine[1]
          this.testCases = firstLine[2]

          let possibleCases = lines.slice(1,this.languageLength)

          let regExpr = [];
          var languageWords = [];
          var solutions = [];

          for(var i=0; i < this.languageLength; i++){ //Add the language that will be tested.
            languageWords.push(lines[i+1]);
          }


          for (var i = 0; i < this.testCases; i++) {

            var j = parseInt(i)+parseInt(this.languageLength)+parseInt(1);

            regExpr.push(lines[j])

            let reLeft = new RegExp("\\(",'g');
            let reRight = new RegExp("\\)",'g');

            regExpr[i] = regExpr[i].replace(reLeft,"[").replace(reRight,"]");

          }

          var result;

          for(var r = 0; r < regExpr.length; r++){
              regExpr[r] = new RegExp(regExpr[r]);
              solutions[r] = 0;
              for(var t = 0; t < languageWords.length; t++){
                if(regExpr[r].test(languageWords[t])){
                  solutions[r]++;
                }
              }
              result += "Case #"+parseInt(r+1)+": "+solutions[r]+"\n";
            }
        }
        console.log(result);

        file.readAsText(input_file)

      }

    }else{
      alert("File API are not fully supported in this browser")
    }
  } // end init

}



let inputFile = "small-input.txt"
let outputFile = "output.txt"

var alien = new Alienn(inputFile, outputFile)

document.getElementById('fileinput').addEventListener('change', alien.init, false);
